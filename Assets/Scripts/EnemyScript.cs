﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    protected EnemyType myType;
    protected EnemyBlock parentBlock = null;
    protected int hitScore;
    protected bool triggerWithWall = true;

    //common trigger control for all types of ships.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //inform block to change moving direction.
        if (collision.gameObject.tag == "GameWall" && triggerWithWall)
        {
            parentBlock.OnHitTheWall();
        }
        else if (collision.gameObject.tag == "PlayerBullet")
        {
            GameController.instance.OnEnemyHitByBullet(transform.position, hitScore);
            parentBlock.OnEnemyHit(myType);
            gameObject.SetActive(false);
        }
    }

    public void SetParentBlock(EnemyBlock enemyBlock)
    {
        parentBlock = enemyBlock;
        myType = EnemyType.normal;
    }

    public virtual void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    protected void OnMoveOutOfScreen()
    {
        parentBlock.OnMoveOutOfScreen(myType);
    }
}
