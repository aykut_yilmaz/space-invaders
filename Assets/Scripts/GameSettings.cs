﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameSettings
{
    //gameplay area from the bottom left to upper right corner.
    public static readonly Rect GameArea = new Rect(-4f, -4f, 8f, 8f);

    public static readonly int MaxPlayerHealth = 3;
}
