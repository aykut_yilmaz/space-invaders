﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFactory : MonoBehaviour
{
    public GameObject bulletPrefab;
    private List<BulletScript> freeBullets;

    private void Start()
    {
        freeBullets = new List<BulletScript>();
    }

    /* If there is a free bullet in the pool use it.
     * Or create new one */
    public void Fire(Vector3 position, bool forPlayer)
    {
        BulletScript bullet;

        if (freeBullets.Count == 0)
        {
            bullet = CreateBullet(position);
        }
        else
        {
            bullet = freeBullets[0];
            bullet.transform.position = position;
            bullet.gameObject.SetActive(true);
            freeBullets.RemoveAt(0);
        }

        if (forPlayer)
        {
            bullet.FireForPlayer();
        }
        else
        {
            bullet.FireForEnemy();
        }
    }

    private BulletScript CreateBullet(Vector3 position)
    {
        GameObject bullet = Instantiate(bulletPrefab, transform);
        bullet.transform.position = position;
        bullet.GetComponent<BulletScript>().SetFactory(this);

        return bullet.GetComponent<BulletScript>();
    }

    /*a bullet hits to an object or goes out of screen bounds,
     * it returns to bullet pool.*/
    public void SetMeFree(BulletScript bullet)
    {
        freeBullets.Add(bullet);
    }
}
