﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mothership : ShootingEnemy
{
    private Vector3 direction;

    // Start is called before the first frame update
    void Start()
    {
        hitScore = 10;
        shootFrequency = 2.0f;
        triggerWithWall = false;
        myType = EnemyType.mothership;

        InitializeMovement();
        StartCoroutine(ShootCoroutine());
    }

    private void Update()
    {
        if (GameController.instance.IsGameRunning())
        {
            transform.Translate(direction * Time.deltaTime);

            if (transform.position.x > GameSettings.GameArea.xMax || transform.position.x < GameSettings.GameArea.xMin)
            {
                OnMoveOutOfScreen();
                gameObject.SetActive(false);
            }
        }
    }

    public void InitializeMovement()
    {
        if (Random.Range(0, 100) < 50)
        {
            direction = Vector3.right;
            transform.position = new Vector3(-4f, 4f, 0);
        }
        else
        {
            direction = Vector3.left;
            transform.position = new Vector3(4f, 4f, 0);
        }
    }
}
