﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalEnemy : EnemyScript
{
    // Start is called before the first frame update
    void Start()
    {
        hitScore = 1;
        myType = EnemyType.normal;
        triggerWithWall = true;
    }
}
