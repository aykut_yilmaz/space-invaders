﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType
{
    normal,
    shooting,
    mothership
}

/// <summary>
/// Create for 6x6 grid and random flying motherships.
/// </summary>
public class EnemyBlock : MonoBehaviour
{
    public GameObject standartEnemyPrefab;
    public GameObject shootingEnemyPrefab;
    public GameObject mothershipPrefab;
    public Transform mothershipHolder;

    private Vector3 movingDirection;
    private float movingSpeed;
    private int activeEnemyCount;
    private int activeMothershipCount;

    private List<EnemyScript> gridEnemies;

    private void Start()
    {
        gridEnemies = new List<EnemyScript>();
        InitializeData();
    }

    private void InitializeData()
    {
        movingDirection = Vector3.right;
        movingSpeed = .5f;
        activeMothershipCount = 0;
        activeEnemyCount = 0;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!GameController.instance.IsGameRunning())
        {
            return;
        }

        transform.Translate(movingDirection * Time.deltaTime * movingSpeed);
    }

    /// <summary>
    /// Create 6x6 enemy grid whether from nothing or
    /// using already created ships.
    /// </summary>
    public void CreateBlock()
    {
        ClearAllData();

        if (gridEnemies.Count == 0)
        {
            InstantiateBlock();
        }
        else
        {
            RestartBlock();
        }

        RemoveMotherships();
        StartCoroutine(CreateMothershipCoroutine());
    }

    /// <summary>
    /// Instantiates ships on grid one by one.
    /// </summary>
    private void InstantiateBlock()
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                GameObject enemy;

                if ((j == 1 || j == 3) && (i == 0 || i == 5))
                {
                    enemy = Instantiate(shootingEnemyPrefab, transform);
                }
                else
                {
                    enemy = Instantiate(standartEnemyPrefab, transform);
                }

                enemy.transform.localPosition =
                    new Vector3(-1.25f + i * 0.5f, 3.5f - j * 0.5f, 0);

                enemy.GetComponent<EnemyScript>().SetParentBlock(this);

                gridEnemies.Add(enemy.GetComponent<EnemyScript>());

                activeEnemyCount++;
            }
        }
    }

    //Reactivate dead ships on grid.
    private void RestartBlock()
    {
        foreach (var item in gridEnemies)
        {
            item.SetActive(true);
        }

        activeEnemyCount = gridEnemies.Count;
    }

    /// <summary>
    /// Can be used when going back to lobby to hide all enemies.
    /// </summary>
    public void HideAllShips()
    {
        foreach (var item in gridEnemies)
        {
            item.SetActive(false);
        }

        RemoveMotherships();
    }

    private void RemoveMotherships()
    {
        foreach (Transform item in mothershipHolder)
        {
            Destroy(item.gameObject);
        }
    }

    //Checks on every 4 seconds to create a mothership.
    private IEnumerator CreateMothershipCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(4.0f);

            if (GameController.instance.IsGameRunning() && Random.Range(0, 100) < 40)
            {
                CreateMothership();
            }
        }
    }

    /// <summary>
    /// Instantiate a mothership.
    /// </summary>
    private void CreateMothership()
    {
        GameObject mothership = Instantiate(mothershipPrefab, mothershipHolder);
        mothership.GetComponent<EnemyScript>().SetParentBlock(this);
        activeMothershipCount++;
    }

    public void ClearAllData()
    {
        ResetPosition();
        StopAllCoroutines();
        InitializeData();
    }

    public void OnHitTheWall()
    {
        /*Multiple end objects can hit the wall. So check for the position
            to avoid conflicts*/
        if ((transform.position.x > 0 && movingDirection == Vector3.right) ||
            (transform.position.x < 0 && movingDirection == Vector3.left))
        {
            movingDirection *= -1;
            movingSpeed *= 1.1f; //move faster
            transform.position = transform.position - new Vector3(0, .25f, 0);
        }
    }

    /*mothership can go outside of the game area,
     * send it back to pool.*/
    public void OnMoveOutOfScreen(EnemyType type)
    {
        if (type == EnemyType.mothership)
        {
            activeMothershipCount--;
        }
    }

    //a ship hit by a bullet.
    public void OnEnemyHit(EnemyType enemyType)
    {
        if (enemyType == EnemyType.normal || enemyType == EnemyType.shooting)
        {
            activeEnemyCount--;

            if (activeEnemyCount <= 0)
            {
                /* All ships on grid destroyed.
                 * But there might be motherships flying.*/
                StartCoroutine(CheckForSuccess());
            }
        }
        else
        {
            activeMothershipCount--;
        }
    }

    /*Checks for the last flying motherships after the destruction
        of grid */ 
    private IEnumerator CheckForSuccess()
    {
        while (true)
        {
            yield return new WaitForSeconds(.5f);

            // No ship left.
            if (activeMothershipCount <= 0)
            {
                GameController.instance.OnAllEnemiesHit();
                StopAllCoroutines();
            }
        }
    }

    private void ResetPosition()
    {
        transform.position = Vector3.zero;
    }
}
