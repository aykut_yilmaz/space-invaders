﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    private float movingSpeed = 1.5f;
	private float distanceFromEdge = .5f;
    private Vector3 initialPosition;

    private void Start()
    {
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameController.instance.IsGameRunning())
        {
            return;
        }

        if (Input.GetKey(KeyCode.LeftArrow) &&
            transform.position.x > GameSettings.GameArea.xMin + distanceFromEdge)
        {
            transform.Translate(Vector3.left * Time.deltaTime * movingSpeed);
        }
        else if (Input.GetKey(KeyCode.RightArrow) &&
            transform.position.x < GameSettings.GameArea.xMax - distanceFromEdge)
        {
            transform.Translate(Vector3.right * Time.deltaTime * movingSpeed);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fire();
        }
    }

    private void Fire()
    {
        GameController.instance.GetBulletFactory().Fire(transform.position, true);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "EnemyBullet")
        {
            GameController.instance.OnPlayerHitByBullet();
        }
        if(collision.gameObject.tag == "EnemyShip")
        {
            GameController.instance.OnPlayerHitByEnemyShip();
        }
    }

    public void ResetPosition()
    {
        transform.position = initialPosition;
    }
}
