﻿
using UnityEngine;
using DG.Tweening;
using TMPro;

public class PointText : MonoBehaviour
{
    private TextMeshProUGUI text;

    // Use this for initialization
    void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    public void Animate(Vector3 pos, int point)
    {
        DOTween.KillAll(transform);

        //reset transform and gameobject data.
        gameObject.SetActive(true);
        transform.localPosition = pos;
        text.text = "+" + point.ToString();

        transform.localScale = new Vector3(1, 0, 1);
        transform.position = pos - new Vector3(0, 50f, 0);
        text.color = Color.white;

        //move and scale action.
        transform.DOScale(Vector3.one, .25f);
        transform.DOLocalMove(transform.localPosition - new Vector3(0, 100, 0), .7f).SetEase(Ease.OutSine);

        Sequence sequence = DOTween.Sequence();
        sequence.AppendInterval(.5f);
        sequence.Append(text.DOFade(0, .5f));

        sequence.AppendCallback(delegate {
            gameObject.SetActive(false);
        });
    }
}
