﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemy : EnemyScript
{
    public GameObject bulletPrefab;
    protected float shootFrequency;

    // Start is called before the first frame update
    void Start()
    {
        hitScore = 2;
        shootFrequency = 2.0f;
        triggerWithWall = true;
        myType = EnemyType.shooting;

        StartCoroutine(ShootCoroutine());
    }

    public override void SetActive(bool active)
    {
        gameObject.SetActive(active);

        if (active)
        {
            StopAllCoroutines();
            StartCoroutine(ShootCoroutine());
        }
    }

    protected IEnumerator ShootCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(shootFrequency);

            if (GameController.instance.IsGameRunning())
            {
                Shoot();
            }
        }
    }

    protected void Shoot()
    {
        GameController.instance.GetBulletFactory().Fire(transform.position, false);
    }
}
