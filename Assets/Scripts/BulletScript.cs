﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    private Vector3 direction;
    private SpriteRenderer sprite;
    private float bulletSpeed;
    private BulletFactory myFactory;

    private void Awake()
    {
        bulletSpeed = 3.0f;
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * Time.deltaTime * bulletSpeed);

        //bullet fired but did not hit anything. So return to factory.
        if (transform.position.y < GameSettings.GameArea.yMin || transform.position.y > GameSettings.GameArea.yMax)
        {
            gameObject.SetActive(false);
            myFactory.SetMeFree(this);
        }
    }

    public void SetFactory(BulletFactory bulletFactory)
    {
        myFactory = bulletFactory;
    }

    public void FireForPlayer()
    {
        direction = Vector3.up;
        gameObject.layer = 8; //Layer: PlayerBullet
        gameObject.tag = "PlayerBullet";
        sprite.color = Color.white;
    }

    public void FireForEnemy()
    {
        direction = Vector3.down;
        gameObject.layer = 9; //Layer: EnemyBullet
        gameObject.tag = "EnemyBullet";
        sprite.color = Color.red;
    }

    //deactivate and inform the factory.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        gameObject.SetActive(false);
        myFactory.SetMeFree(this);
    }
}
