﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    inLobby,
    inGame
}

public class GameController : MonoBehaviour
{
    private GameState gameState;
    public EnemyBlock enemyBlock;
    public UIController uIController;
    public BulletFactory bulletFactory;
    public PlayerScript playerScript;

    public static GameController instance;

    private int score;
    private int playerHealth;
    private Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        mainCamera = Camera.main;
        gameState = GameState.inLobby;
    }

    public bool IsGameRunning()
    {
        return gameState == GameState.inGame;
    }

    public void StartGame()
    {
        PrepareGame();
        StartCoroutine(StartOnCountdown());
    }

    public void GoToLobby()
    {
        enemyBlock.HideAllShips();
        playerScript.ResetPosition();
        uIController.ShowLobbyPanel();
    }

    public void RestartGame()
    {
        PrepareGame();
        StartCoroutine(StartOnCountdown());
    }

    private void PrepareGame()
    {
        enemyBlock.CreateBlock();
        playerScript.ResetPosition();

        score = 0;
        playerHealth = GameSettings.MaxPlayerHealth;

        uIController.PrepareGame();
        uIController.SetHealth(playerHealth);
        uIController.SetScore(score);
    }

    //show countdown then start the game.
    private IEnumerator StartOnCountdown()
    {
        uIController.ShowCountdown();
        yield return new WaitForSeconds(3.5f);
        gameState = GameState.inGame;
    }

    private void ContinueGame()
    {
        playerScript.ResetPosition();
        StartCoroutine(StartOnCountdown());
    }

    private void PlayerFailed()
    {
        gameState = GameState.inLobby;
        uIController.ShowGameFail();
    }

    public void OnAllEnemiesHit()
    {
        if (IsGameRunning())
        {
            gameState = GameState.inLobby;
            uIController.ShowGameSuccess();
        }
    }

    /* Manager checks the health and game
     * condition after player gets hit*/
    public void OnPlayerHitByBullet()
    {
        if (gameState == GameState.inGame)
        {
            gameState = GameState.inLobby;

            playerHealth--;
            uIController.SetHealth(playerHealth);

            if (playerHealth > 0)
            {
                ContinueGame();
            }
            else
            {
                PlayerFailed();
            }
        }
    }

    //instant lose.
    public void OnPlayerHitByEnemyShip()
    {
        if (gameState == GameState.inGame)
        {
            PlayerFailed();
        }
    }

    //player gains score.
    public void OnEnemyHitByBullet(Vector3 hitPosition, int points)
    {
        score += points;
        uIController.SetScore(score);
        uIController.ShowPoint(mainCamera.WorldToScreenPoint(hitPosition), points);
    }


    public BulletFactory GetBulletFactory()
    {
        return bulletFactory;
    }
}
