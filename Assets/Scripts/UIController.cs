﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIController : MonoBehaviour
{
    [Header("Panels")]
    public GameObject lobbyPanel;
    public GameObject gamePanel;
    public GameObject endPanel;

    [Header("Buttons")]
    public GameObject playButton;

    [Header("Texts")]
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI countdownText;
    public TextMeshProUGUI endMessageText;

    [Header("Pool")]
    public List<TextMeshProUGUI> pointPool;
    public List<GameObject> heartList;

    private int pointPoolIndex;

    public void PrepareGame()
    {
        pointPoolIndex = 0;

        foreach (var item in pointPool)
        {
            item.gameObject.SetActive(false);
        }

        ShowGamePanel();
    }

    public void ShowLobbyPanel()
    {
        endPanel.SetActive(false);
        gamePanel.SetActive(false);
        lobbyPanel.SetActive(true);
    }

    public void ShowGamePanel()
    {
        endPanel.SetActive(false);
        gamePanel.SetActive(true);
        lobbyPanel.SetActive(false);
    }

    public void ShowGameFail()
    {
        endMessageText.SetText("Failed!");
        endPanel.SetActive(true);
    }

    public void ShowGameSuccess()
    {
        endMessageText.SetText("Success!");
        endPanel.SetActive(true);
    }

    public void SetScore(int score)
    {
        scoreText.SetText(score.ToString());
    }

    public void SetHealth(int health)
    {
        for (int i = 0; i < heartList.Count; i++)
        {
            heartList[i].SetActive(health > i);
        }
    }

    public void ShowPoint(Vector3 pos, int points)
    {
        pointPoolIndex++;
        pointPoolIndex = pointPoolIndex % pointPool.Count;

        pointPool[pointPoolIndex].gameObject.SetActive(true);
        pointPool[pointPoolIndex].GetComponent<PointText>().Animate(pos, points);
    }

    public void ShowCountdown()
    {
        StartCoroutine(AnimateCountdown());
    }

    private IEnumerator AnimateCountdown()
    {
        int count = 3;
        countdownText.gameObject.SetActive(true);

        while (count > 0)
        {
            countdownText.SetText(count.ToString());
            count--;

            yield return new WaitForSeconds(1.0f);
        }

        countdownText.gameObject.SetActive(false);
    }

    public void OnClickPlay()
    {
        GameController.instance.StartGame();
    }

    public void OnClickLobby()
    {
        GameController.instance.GoToLobby();
    }

    public void OnClickRestart()
    {
        GameController.instance.RestartGame();
    }
}
